FROM python:2.7.14
MAINTAINER Jon K Hellan <hellan@acm.org>
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -qqy update && apt-get -qqy dist-upgrade \
    && apt-get -qqy install --no-install-recommends libsqlite3-dev gpsbabel \
    && apt-get -qqy autoremove && apt-get -qqy clean all
RUN mkdir -p /app/
WORKDIR /app/
RUN git clone https://jhellan@bitbucket.org/jhellan/rman.git \
    && rm -rf /app/rman/.git \
    && pip2 install -r /app/rman/requirements.txt
VOLUME /db
VOLUME /app/rman/webapp/data/tiles
COPY testdata/rman.db /db/
EXPOSE 8080
CMD ["env", "PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/app/rman/bin", "python", "/app/rman/webapp/rman.py", "--appdir", "/app/rman/webapp", "--dbdir", "/db"]
