<%inherit file="main.tpl"/>
<div class="jumbotron">
	<h2>Routes</h2>
</div>
&nbsp;
<table class="table table-condensed">
  <thead><tr>
    <th class="hidden-xs">Name</th>
    <th class="hidden-xs">Length</th>
    <th class="hidden-xs">When added</th>
    <th class="hidden-xs">Sent to devices</th>
  </tr></thead>
  <tbody>
% for route in sorted(recents, key=lambda r: r.timestamp, reverse=True) + sorted(oldies, key=lambda r: r.name.lower()):
        <tr><td>
	  <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
%if route in recents:
              <span class="glyphicon glyphicon-star"></span>
%endif
              ${route.name}
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li class="uploaditem"><a
	      href="upload?rtid=${route.rtid}">Send to GPS</a></li>
              <li><a href="get_gpx?rtid=${route.rtid}">Export</a></li>
              <li><a href="preview?rtid=${route.rtid}">Preview</a></li>
              <li><a href="rename_ui?rtid=${route.rtid}">Rename</a></li>
            </ul>
          </div>
	</td>
	<td>${"%.1f NM" % route.length}</td>
	<td class="hidden-xs">${route.timestamp.strftime('%Y-%m-%d')}</td>
	<td class="hidden-xs">${route.uploads}</td>
</tr>
% endfor
</tbody>
</table>
