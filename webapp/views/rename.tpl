<%inherit file="main.tpl"/>
<div class="jumbotron">
<h2>Route ${route.name}</h2>
</div>
&nbsp;
<form name=input action="rename" enctype="multipart/form-data" method="post">
<div class="form-group>
<label for="newname">New name:</label>
<input type="hidden" id="rtid" name="rtid" value="${route.rtid}">
<input type="text" class="form-control" id="newname" name="newname">
</div>
<button type="submit" class="btn btn-default btn-primary">Rename route</button>
</form>
