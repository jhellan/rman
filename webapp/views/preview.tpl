<%inherit file="main.tpl"/>
<div class="jumbotron">
<h2>Route ${route.name}</h2>
</div>
     <script src="/data/OpenLayers-2.13.1/OpenLayers.js"></script>
<style type="text/css">
      #map {
      }
    div.olMap {
        height: 500px;
    }
</style>
&nbsp;
<div id="map"></div>
<script>
function get_bbox() {
    var lonextent      = ${maxlon} - ${minlon};
    var latextent      = ${maxlat} - ${minlat};
    var extra          = 1.4;// Space around route
    var midlon         = (${maxlon} + ${minlon})/2;
    var midlat         = (${maxlat} + ${minlat})/2;
    return new OpenLayers.Bounds(midlon - lonextent*extra/2., midlat - latextent*extra/2.,
                                 midlon + lonextent*extra/2., midlat + latextent*extra/2.);
}

function init() {
    map = new OpenLayers.Map("map", { controls: [] });
    var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
    var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
    var bounds         = get_bbox().transform(fromProjection, toProjection);

    var newLayer = new OpenLayers.Layer.OSM(
	"Tiles from Eniro nautical",
<%text>
	"/data/tiles/${z}/${x}/${y}.png",
</%text>
	{
	    alpha: true,
	    minZoomLevel: 9, 
	    maxZoomLevel: 9, 
	    isBaseLayer: true,
	    attribution: "Map (c) Eniro"
	}
    );
    map.addLayer(newLayer);

    var lgpx = new OpenLayers.Layer.Vector("${route.name}", {
	strategies: [new OpenLayers.Strategy.Fixed()],
	protocol: new OpenLayers.Protocol.HTTP({
	    url: "get_gpx?rtid=${route.rtid}",
	    format: new OpenLayers.Format.GPX()
	}),
	style: {strokeColor: "blue", strokeWidth: 5, strokeOpacity: 0.8},
	projection: new OpenLayers.Projection("EPSG:4326")
    });
    map.addLayer(lgpx);
    map.addControl(new OpenLayers.Control.ScaleLine({
	geodesic: true,
	topOutUnits: "nmi",
	bottomOutUnits: "km",
	topInUnits: "m",
	bottomInUnits: "m"}));
    map.addControl(new OpenLayers.Control.Attribution());
    map.zoomToExtent(bounds, true);
    map.zoomTo(9);
}

$(document).ready(function() {
  init();
});
</script>
