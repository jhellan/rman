<%inherit file="main.tpl"/>
<div class="jumbotron">
<h2>Import route from link</h2>
</div>
&nbsp;
<form name=input action="record_url" enctype="multipart/form-data" method="post">
<div class="form-group>
<label for="name">Route name:</label>
<input type="text" class="form-control" id="name" name="name" placeholder="Optional">
</div>
<div class="form-group>
<label for="url">Link URL:</label>
<input type="url" class="form-control" id="url" name="url">
</div>
<button type="submit" class="btn btn-default btn-primary">Import route</button>
</form>
