<%inherit file="main.tpl"/>
<div class="jumbotron">
<h2>Import route from file</h2>
</div>
&nbsp;
<form name=input action="record_gpx" enctype="multipart/form-data" method="post">
<div class="form-group>
<label for="name">Route name:</label>
<input type="text" class="form-control" id="name" name="name" placeholder="Optional">
</div>
<p>File to import: <input type="file" name="gpx"></p>
<button type="submit" class="btn btn-default btn-primary">Import route</button>
</form>
