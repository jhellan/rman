<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="data/favicon.ico">
	<title>Route Manager</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="/data/bootstrap/css/bootstrap.css"/>

	<!-- Custom styles for this template -->
	<link href="/data/style.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>

<body>

	<script src="/data/jquery-1.10.1.min.js"></script>
	<script type="text/javascript">
function update_model(data) {
    "use strict";

    var connlbltxt = document.querySelector('#connlbltxt');
    var connlabel = document.querySelector('#connlabel');
    var connicon = document.querySelector('#connicon');
    var uploaditems = document.querySelectorAll('.uploaditem');

    if (data.model) {
        connlbltxt.innerHTML = "Connected to " + data.model;
        connlabel.classList.add("label-success");
        connlabel.classList.remove("label-warning");
        connicon.classList.add("icon-ok");
        connicon.classList.remove("icon-remove");
        for (var i = 0; i < uploaditems.length; i++) {
	    uploaditems[i].classList.remove("disabled");
        }
    } else {
        connlbltxt.innerHTML = "No connected GPS";
        connlabel.classList.remove("label-success");
        connlabel.classList.add("label-warning");
        connicon.classList.remove("icon-ok");
        connicon.classList.add("icon-remove");
        for (var i = 0; i < uploaditems.length; i++) {
	    uploaditems[i].classList.add("disabled");
        }
    }
    window.setTimeout(function () {$.ajax({ url: "get_model", success: update_model }); }, 1000);
}
        </script>
	<!-- Fixed navbar -->
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand"
				href="#"><img height="30 px" src="data/boat.svg" alt="Boat" type="image/svg"></a>
				
			</div>
			<div class="navbar-department">
				<div class="department">Route Manager</div>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="index">List routes</a></li>
					<li><a href="save_url">Import
					route from link</a></li>
					<li><a href="save_route">Import
					route from file</a></li>
					<li><a href="shut_down"><span class="glyphicon
					glyphicon-off"></span> Shut down</a></li>
				</ul>
			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="jk-color-white jk-padded gutter">
                              <span id="connlabel" class="label label-success"><i id="connicon" class="icon-ok icon-white"></i>
                              <span id="connlbltxt">No connected GPS</span></span>
	<script type="text/javascript">
$.ajax({ url: "get_model", success: update_model });
        </script>
			  ${self.body()}
				</div>
			</div>
		</div>
	</div>

	<!-- /container --> 

	<!-- Bootstrap core JavaScript --> 
	<!-- Placed at the end of the document so the pages load faster --> 
	<script src="/data/bootstrap/js/bootstrap.min.js"></script>


</body></html>
