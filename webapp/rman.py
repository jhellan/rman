#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import datetime
import logging
import os
import os.path
import string
import subprocess
import traceback
import urllib
from math import radians, cos, sin, asin, sqrt
from uuid import uuid4
from xml.etree import ElementTree

import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column
from sqlalchemy.types import Integer, Float, DateTime, UnicodeText, LargeBinary
from mako.lookup import TemplateLookup
import cherrypy
from cherrypy.process import plugins
import gpxpy
import gpxpy.gpx

KMLTYPE = 'application/vnd.google-earth.kml+xml'
KMZTYPE = 'application/vnd.google-earth.kmz'
GPXTYPE = 'application/gpx+xml'
EXTENSIONS = {
    KMLTYPE: '.kml',
    KMZTYPE: '.kmz',
    GPXTYPE: '.gpx'
}

# Helper to map and register a Python class a db table
Base = declarative_base()  # pylint: disable=C0103


def get_namespace(element):
    firstpart, _, _ = element.tag.partition('}')
    return firstpart[1:]


def make_tag(namespace, tagname):
    return '{{{}}}{}'.format(namespace, tagname)


def rename_in_gpx(gpxstr, newname):
    # gpxpy loses bounding box, so cannot use gpxpy
    root = ElementTree.fromstring(gpxstr)
    namespace = get_namespace(root)
    ElementTree.register_namespace('', namespace)
    route_gpx = root.find(make_tag(namespace, 'rte'))
    name_gpx = route_gpx.find(make_tag(namespace, 'name'))
    name_gpx.text = newname
    bounds_gpx = root.find(make_tag(namespace, 'bounds'))
    if bounds_gpx is None:
        bounds_gpx = Bounds().to_gpx(namespace)
        root.insert(1, bounds_gpx)
    Bounds.from_route(route_gpx, namespace).update_gpx(bounds_gpx)
    return ElementTree.tostring(root)


class Bounds(object):
    def __init__(self, minlat=91., minlon=181., maxlat=-91., maxlon=-181.):
        self.minlat = minlat
        self.minlon = minlon
        self.maxlat = maxlat
        self.maxlon = maxlon

    def update(self, lat, lon):
        self.minlat = min(self.minlat, lat)
        self.minlon = min(self.minlon, lon)
        self.maxlat = max(self.maxlat, lat)
        self.maxlon = max(self.maxlon, lon)

    def to_gpx(self, namespace):
        return ElementTree.Element(
            make_tag(namespace, 'bounds'),
            attrib={
                'maxlat': str(self.maxlat),
                'maxlon': str(self.maxlon),
                'minlat': str(self.minlat),
                'minlon': str(self.minlon)
            })

    def update_gpx(self, bounds_gpx):
        bounds_gpx.attrib['maxlat'] = str(self.maxlat)
        bounds_gpx.attrib['maxlon'] = str(self.maxlon)
        bounds_gpx.attrib['minlat'] = str(self.minlat)
        bounds_gpx.attrib['minlon'] = str(self.minlon)

    @staticmethod
    def from_route(route_gpx, namespace):
        bounds = Bounds()
        for child in route_gpx.iter(make_tag(namespace, 'rtept')):
            bounds.update(float(child.attrib['lat']), float(child.attrib['lon']))
        return bounds


class Route(Base):
    __tablename__ = 'route'
    rtid = Column('id', Integer, primary_key=True)
    name = Column(UnicodeText)
    length = Column(Float)
    timestamp = Column(DateTime)
    uploads = Column(Integer)
    gpx = Column(UnicodeText)
    minimap = Column(LargeBinary)

    def __init__(self, name, length, gpx):
        Base.__init__(self)
        self.name = name
        self.length = length
        self.timestamp = datetime.datetime.now()
        self.uploads = 0
        self.gpx = rename_in_gpx(gpx, name)

    def __str__(self):
        return self.name.encode('utf-8')

    def __unicode__(self):
        return self.name

    def rename(self, newname):
        self.name = newname
        self.gpx = rename_in_gpx(self.gpx, newname)

    @staticmethod
    def list(session):
        return session.query(Route).all()

    @staticmethod
    def get_by_rtid(session, rtid):
        return session.query(Route).filter_by(rtid=rtid)

    @staticmethod
    def get_by_name(session, name):
        return session.query(Route).filter_by(name=name)


def log(msg, context='RMAN', severity=logging.INFO, show_trace=False):
    cherrypy.log(msg, context, severity, show_trace)


def log_error(exc, place=None, msg=None, context='RMAN', severity=logging.ERROR, show_trace=True):
    emsg = u"Error in {0}: {1}".format(place, exc)
    if msg:
        emsg = emsg + ": " + msg
    emsg = emsg+"\n"
    cherrypy.log.error(emsg, context, severity, show_trace)


def safe_filename(name):
    # Filter downcases, than removes everything outside "-_a-z0-9"
    safechars = string.digits+string.letters+"_-"
    return "".join(s for s in name if s in safechars).lower()


def make_charmap(invcharmap):
    res = {}
    for k, v in invcharmap.items():
        for c in v:
            res[c] = k
    return res


# Make a routename suitable for pre Unicde Garmin
def safe_routename(name):
    # Upcases, strips accents etc, then removes everything outside "_-+ a-z0-9 "
    safechars = string.digits+string.letters+"_-+ "
    invcharmap = {
        "A": u"ÀÁÂÃÄÅÆ",
        "C": u"Ç",
        "D": u"Ð",
        "E": u"ÈÉÊË",
        "I": u"ÌÍÎÏ",
        "N": u"Ñ",
        "O": u"ÒÓÔÖÖØ",
        "S": u"ß",
        "T": u"Þ",
        "U": u"ÙÚÛÜ",
        "Y": u"Ý"
    }
    charmap = make_charmap(invcharmap)
    routename = ""
    for c in name.upper():
        if c in charmap:
            routename += charmap[c]
        elif c in safechars:
            routename += c
    return routename


def msg_as_url(msg1="", msg2=""):
    umsg1 = urllib.quote_plus(msg1.encode('utf8'))
    umsg2 = urllib.quote_plus(msg2.encode('utf8'))
    return "/message?msg1={0}&msg2={1}".format(umsg1, umsg2)


def kml2gpx(kmlstr, filename):
    dir_ = os.path.dirname(filename)
    with open(filename, "w") as kmlf:
        kmlf.write(kmlstr)
    cmd = ['kmz2gpx', filename]
    log('Conversion command: "{0}"'.format(str(cmd)))
    gpxfile = dir_ + '/' + subprocess.check_output(cmd).strip()
    return gpxfile


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = [radians(x) for x in (lon1, lat1, lon2, lat2)]

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    hav = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    angle = 2 * asin(sqrt(hav))

    # 6367 km is the radius of the Earth
    dist = 6367 * angle
    return dist


def compute_length(gpxstr):
    gpx = gpxpy.parse(gpxstr)

    last = None
    length = 0
    for point in gpx.routes[0].points:
        if last:
            length += haversine(last.longitude, last.latitude, point.longitude, point.latitude)
        last = point
    return length / 1.852  # In nm


class Gpsunit(object):
    config = {'rundir': '.',
              'fileext': 'gps'}

    @staticmethod
    def set_config(config):
        Gpsunit.config = config

    @staticmethod
    def get_dir():
        rdir = Gpsunit.config['rundir']
        try:
            dirs = [dir_ for dir_ in os.listdir(rdir)
                    if dir_.endswith('.' + Gpsunit.config['fileext'])]
            return rdir+"/"+dirs[0]
        except (LookupError, TypeError, IOError, OSError):
            return None

    @staticmethod
    def get_model():
        try:
            dir_ = Gpsunit.get_dir()
            with open(dir_ + "/model.txt") as mfh:
                model = mfh.read().rstrip()
                return model
        except (IOError, TypeError, OSError):
            return None

    @staticmethod
    def upload(route):
        log(u"Gpsunit.upload route \"{0}\" starting".format(route.name))
        gpxstr = rename_in_gpx(route.gpx, safe_routename(route.name))
        try:
            dir_ = Gpsunit.get_dir()
            filename = dir_ + "/" + safe_filename(route.name) + ".gpx"
            with open(filename, "w") as gpxf:
                gpxf.write(str(gpxstr))
            log(u"Gpsunit.upload completed, wrote file {0}".format(filename))
        except Exception as exc:
            log_error(exc, "Gpsunit.upload")
            raise


class SAEnginePlugin(plugins.SimplePlugin):
    def __init__(self, bus, connection_string=None):
        """
        The plugin is registered to the CherryPy engine and therefore
        is part of the bus (the engine *is* a bus) registery.

        We use this plugin to create the SA engine. At the same time,
        when the plugin starts we create the tables into the database
        using the mapped class of the global metadata.

        Finally we create a new 'bind' channel that the SA tool
        will use to map a session to the SA engine at request time.
        """
        plugins.SimplePlugin.__init__(self, bus)
        self.sa_engine = None
        self.connection_string = connection_string
        self.bus.subscribe("bind", self.bind)

    def start(self):
        self.sa_engine = create_engine(self.connection_string, echo=False)
        Base.metadata.create_all(self.sa_engine)

    def stop(self):
        if self.sa_engine:
            self.sa_engine.dispose()
            self.sa_engine = None

    def bind(self, session):
        session.configure(bind=self.sa_engine)


class SATool(cherrypy.Tool):
    def __init__(self):
        """
        The SA tool is responsible for associating a SA session
        to the SA engine and attaching it to the current request.
        Since we are running in a multithreaded application,
        we use the scoped_session that will create a session
        on a per thread basis so that you don't worry about
        concurrency on the session object itself.

        This tools binds a session to the engine each time
        a requests starts and commits/rollbacks whenever
        the request terminates.
        """
        cherrypy.Tool.__init__(self, 'on_start_resource',
                               self.bind_session,
                               priority=20)

        self.session = scoped_session(sessionmaker(autoflush=True,
                                                   autocommit=False))

    def _setup(self):
        cherrypy.Tool._setup(self)
        cherrypy.request.hooks.attach('on_end_resource',
                                      self.commit_transaction,
                                      priority=80)

    def bind_session(self):
        cherrypy.engine.publish('bind', self.session)
        cherrypy.request.db = self.session

    def commit_transaction(self):
        cherrypy.request.db = None
        try:
            self.session.commit()
        except Exception:
            self.session.rollback()
            raise
        finally:
            self.session.remove()


class Root(object):
    def __init__(self, appdir):
        self.lookup = TemplateLookup(directories=os.path.join(appdir, "views"),
                                     input_encoding='utf-8')

    @cherrypy.expose
    def index(self):
        template = self.lookup.get_template("index.tpl")
        routes = Route.list(cherrypy.request.db)
        now = datetime.datetime.now()
        six_hrs = datetime.timedelta(0, 3600*6)
        recents = [r for r in routes if now - r.timestamp < six_hrs]
        oldies = [r for r in routes if now - r.timestamp >= six_hrs]
        return template.render(recents=recents, oldies=oldies)

    @cherrypy.expose
    def save_route(self):
        template = self.lookup.get_template("save_route.tpl")
        return template.render()

    @cherrypy.expose
    def save_url(self):
        template = self.lookup.get_template("save_url.tpl")
        return template.render()

    @cherrypy.expose
    def shut_down(self):
        cmd = ["sudo", "shutdown", "-P", "now"]
        try:
            log(u'shutdown requested')
            res = subprocess.check_output(cmd)
            if res:
                log(u'Shutdown command output was "{0}"'.format(res))
        except Exception as exc:
            log(u'Shutdown failed - "{0}"'.format(exc))
            msg1 = u"Shutdown failed"
            msg2 = u"<pre>{0}</pre>".format(traceback.format_exc())
            raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))

        return self.message("Shutting down", res)

    @cherrypy.expose
    def preview(self, rtid=None):
        routes = Route.get_by_rtid(cherrypy.request.db, rtid)
        if routes.count() < 1:
            raise cherrypy.HTTPRedirect(msg_as_url(u"Route not found"))
        route = routes[0]
        gpx = route.gpx
        etree = ElementTree.fromstring(gpx)
        namespace = get_namespace(etree)
        route_gpx = etree.find(make_tag(namespace, 'rte'))
        bounds = Bounds.from_route(route_gpx, namespace).to_gpx(namespace)
        template = self.lookup.get_template("preview.tpl")
        return template.render(route=route,
                               minlat=bounds.attrib['minlat'],
                               minlon=bounds.attrib['minlon'],
                               maxlat=bounds.attrib['maxlat'],
                               maxlon=bounds.attrib['maxlon'])

    @cherrypy.expose
    def rename_ui(self, rtid=None):
        routes = Route.get_by_rtid(cherrypy.request.db, rtid)
        if routes.count() < 1:
            raise cherrypy.HTTPRedirect(msg_as_url(u"Route not found"))
        route = routes[0]
        template = self.lookup.get_template("rename.tpl")
        return template.render(route=route)

    @staticmethod
    @cherrypy.expose
    def get_gpx(rtid=None):
        routes = Route.get_by_rtid(cherrypy.request.db, rtid)
        if routes.count() < 1:
            raise cherrypy.HTTPRedirect(msg_as_url(u"Route not found"))
        route = routes[0]
        cherrypy.response.headers["Content-Type"] = GPXTYPE
        cdisp = 'attachment; filename="{}.gpx"'.format(safe_filename(route.name))
        cherrypy.response.headers["Content-Disposition"] = cdisp
        return route.gpx.encode('utf-8')

    @staticmethod
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_model():
        mygps = {"model": Gpsunit.get_model()}
        return mygps

    @cherrypy.expose
    def message(self, msg1="", msg2=""):
        template = self.lookup.get_template("message.tpl")
        return template.render(msg1=msg1, msg2=msg2)

    @staticmethod
    @cherrypy.expose
    def record_gpxstr(name=None, length=None, gpxstr=None):
        # go to e.g. /record_gpxstr?name=Fillan - Hopsjo to record a route
        log(str(gpxstr))
        cherrypy.response.headers['content-type'] = 'text/plain'
        try:
            now = datetime.datetime.now()
            if not length:
                length = compute_length(gpxstr)
            if (not name) or (name == "Route"):
                name = "Route {0}".format(now.strftime("%Y-%m-%d %H:%M:%S"))
            routes = Route.get_by_name(cherrypy.request.db, name)
            if routes.count() < 1:
                rte = Route(name, length, gpxstr)
                cherrypy.request.db.add(rte)
                log(u"Added new route rtid={0} {1}".format(rte.rtid, rte.name))
            else:
                rte = routes[0]
                rte.length = length
                rte.gpx = gpxstr
                rte.timestamp = now
                rte.uploads = 0
                log(u"Updated route rtid={0} {1}".format(rte.rtid, rte.name))
            msg1 = u"Recorded: %s" % rte.name
            msg2 = u""
        except Exception as exc:
            log_error(exc, "Recording failed")
            msg1 = u"Recording failed"
            msg2 = u"<pre>{0}</pre>".format(traceback.format_exc())
        raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))

    @staticmethod
    @cherrypy.expose
    def record_gpx(name=None, length=None, gpx=None):
        # go to e.g. /record?name=Fillan - Hopsjo to record a route
        log(str(gpx))
        cherrypy.response.headers['content-type'] = 'text/plain'
        try:
            gpxstr = gpx.file.read()
        except Exception as exc:
            log_error(exc, "Recording failed")
            msg1 = u"Recording failed"
            msg2 = u"<pre>{0}</pre>".format(traceback.format_exc())
            raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))
        Root.record_gpxstr(name, length, gpxstr)

    @staticmethod
    def download_route(url):
        try:
            req = requests.get(url, timeout=3)
            log("{}, {}".format(req.status_code, req.headers['content-type']))
        except requests.exceptions.Timeout:
            msg1 = u"Connection timed out"
            msg2 = u""
            raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))
        except requests.exceptions.ConnectionError as exc:
            msg1 = u"Connection error"
            msg2 = unicode(exc)
            raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))
        except Exception as exc:
            log_error(exc, "Recording failed")
            msg1 = u"Recording failed"
            msg2 = u"<pre>{0}</pre>".format(traceback.format_exc())
            raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))
        if req.status_code != requests.codes.ok:
            msg1 = u"Status from webserver: {0}".format(req.status_code)
            msg2 = unicode(req.reason)
            raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))
        return req

    @staticmethod
    def record_kmlstr(name, kmlstr, ctype):
        kmlfile = '/tmp/' + str(uuid4()) + EXTENSIONS[ctype]
        gpxfile = ''
        try:
            gpxfile = kml2gpx(kmlstr, kmlfile)
        except Exception as exc:
            msg1 = u"File conversion failed: {0}".format(exc)
            msg2 = u"<pre>{0}</pre>".format(traceback.format_exc())
            raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))
        try:
            with open(gpxfile) as gfh:
                gpxstr = gfh.read()
            Root.record_gpxstr(name, None, gpxstr)
        finally:
            os.remove(kmlfile)
            if gpxfile:
                os.remove(gpxfile)

    @staticmethod
    @cherrypy.expose
    def record_url(name=None, url=None):
        # go to e.g. /record_url?name=Fillan - Hopsjo to record a route
        log(str(url))
        req = Root.download_route(url)
        ctype = req.headers['content-type']
        if ctype == GPXTYPE:
            Root.record_gpxstr(name, None, req.content)
        elif ctype in [KMLTYPE, KMZTYPE]:
            Root.record_kmlstr(name, req.content, ctype)
        else:
            msg1 = u"Not a route file"
            msg2 = u"File type: '{}', URL: '{}'".format(ctype, url)
            raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))

    @staticmethod
    @cherrypy.expose
    def rename(rtid=None, newname=None):
        log(u"Rename {0} to {1}".format(rtid, newname))
        cherrypy.response.headers['content-type'] = 'text/plain'
        try:
            routes = Route.get_by_rtid(cherrypy.request.db, rtid)
            if routes.count() < 1:
                msg1 = u"Did not find route id {0}".format(rtid)
                msg2 = u""
                raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))
            rte = routes[0]
            oldname = rte.name
            existing_routes = Route.get_by_name(cherrypy.request.db, newname)
            if existing_routes.count() < 1:
                rte.rename(newname)
                log(u"Route {0} renamed to {1}".format(oldname, rte.name))
                msg1 = u"Renamed {0} to {1}".format(oldname, rte.name)
                msg2 = u""
            else:
                msg1 = u"Name {0} already in use".format(newname)
                msg2 = u""
        except Exception as exc:
            log_error(exc, "Renaming failed")
            msg1 = u"Renaming failed"
            msg2 = u"<pre>{0}</pre>".format(traceback.format_exc())
        raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))

    @staticmethod
    @cherrypy.expose
    def upload(rtid=None):
        log(u"Root.upload route rtid=\"{0}\" starting".format(rtid))
        mygps = Gpsunit.get_model()
        if not mygps:
            msg = msg_as_url(u"No connected GPS", u"Connect/turn on GPS and try again")
            raise cherrypy.HTTPRedirect(msg)
        routes = Route.get_by_rtid(cherrypy.request.db, rtid)
        if routes.count() < 1:
            raise cherrypy.HTTPRedirect(msg_as_url(u"Route not found"))
        try:
            Gpsunit.upload(routes[0])
            routes[0].uploads = routes[0].uploads + 1
            log(u"Root.upload route rtid=\"{0}\" completed".format(rtid))
            msg1 = u"Route transferred to GPS"
            msg2 = u""
        except Exception:
            msg1 = u"Could not transfer route to GPS"
            msg2 = u"<pre>{0}</pre>".format(traceback.format_exc())
        raise cherrypy.HTTPRedirect(msg_as_url(msg1, msg2))


DESCRIPTION = "GPS route management webapp"


def parse_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--appdir', default=os.getcwd(),
                        help='Webapp directory')
    parser.add_argument('--rundir', default='/var/run/rman',
                        help='Runfile directory')
    parser.add_argument('--dbdir', help='Database directory')
    parser.add_argument('--logdir', help='Log directory')
    parser.add_argument('--host', default='0.0.0.0', help='Listening host')
    parser.add_argument('--port', default=8080, type=int,
                        help='Listening port')
    return parser.parse_args()


def main():
    args = parse_args()
    dbdir = args.appdir
    if args.dbdir:
        dbdir = args.dbdir
    db_path = os.path.abspath(os.path.join(dbdir, 'rman.db'))
    connection_string = 'sqlite:///{0}'.format(db_path)
    Gpsunit.set_config({'rundir': args.rundir,
                        'fileext': 'gps'})
    SAEnginePlugin(cherrypy.engine, connection_string).subscribe()
    cherrypy.config.update({'server.socket_port': args.port,
                            'server.socket_host': args.host})
    if args.logdir:
        cherrypy.config.update({'log.access_file': os.path.join(args.logdir, 'access.log'),
                                'log.error_file': os.path.join(args.logdir, 'error.log'),
                                'log.screen': False})

    cherrypy.tools.db = SATool()
    cherrypy.tree.mount(Root(args.appdir), '',
                        {'/': {'tools.db.on': True},
                         '/data': {'tools.staticdir.on': True,
                                   'tools.staticdir.dir': os.path.join(args.appdir, 'data'),
                                   'tools.staticdir.content_types': {'css': 'text/css'}}})
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    main()
