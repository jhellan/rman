#!/bin/sh
#
# rman initscript
#
# Based on the mediatomb debian package.
# Original authors: Jon K Hellan <hellan@acm.org>
#
### BEGIN INIT INFO
# Provides:          rman
# Required-Start:    $network $local_fs
# Required-Stop::    $network $local_fs
# Should-Start:      $all
# Should-Stop:       $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start rman at boot time
# Description:       Manage the rman service to manage routes for GPS units.
### END INIT INFO

unset USER

DESC="rman GPS route service"
NAME=rman

BINDIR=/usr/local/lib/$NAME/bin

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/sbin:/usr/sbin:/bin:/usr/bin:$BINDIR

APPDIR=/usr/local/lib/$NAME/webapp
DBDIR=/var/lib/$NAME
RUNDIR=/var/run/$NAME
LOGDIR=/var/log/$NAME

DAEMON=$APPDIR/rman.py
PIDDIR=/run/$NAME
PIDFILE=$PIDDIR/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME
DEFAULT=/etc/default/$NAME

# Exit if the package is not installed
[ -x $DAEMON ] || exit 0

# Read configuration variable file if it is present
[ -r $DEFAULT ] && . $DEFAULT

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.0-6) to ensure that this file is present.
. /lib/lsb/init-functions

# Run as `rman' if USER is not specified or is `root'
if [ -z $USER ]; then
	USER=rman
fi

# If no group is specified, use USER
if [ -z $GROUP ]; then
	GROUP=$USER
fi

DAEMON_OPTS="--appdir $APPDIR --dbdir $DBDIR --rundir $RUNDIR --logdir $LOGDIR"
DAEMON_ARGS="$DAEMON_OPTS"

#
# Function that starts the daemon/service
#
do_start()
{
	# Return
	#   0 if daemon has been started
	#   1 if daemon was already running
	#   2 if daemon could not be started
	if [ ! -d $LOGDIR ]; then
	    mkdir $LOGDIR || return 2
	fi
	chown $USER:$GROUP $LOGDIR || return 2
	if [ ! -d $PIDDIR ]; then
	    mkdir $PIDDIR || return 2
	fi
	chown $USER:$GROUP $PIDDIR || return 2
	if [ ! -d $RUNDIR ]; then
	    mkdir $RUNDIR || return 2
	fi
	chown $USER:$GROUP $RUNDIR || return 2
	if [ ! -d $DBDIR ]; then
	    mkdir $DBDIR || return 2
	fi
	chown $USER:$GROUP $DBDIR || return 2

	start-stop-daemon --background --start --quiet --make-pidfile --pidfile $PIDFILE \
		--chuid $USER:$GROUP --exec $DAEMON --test > /dev/null \
		|| return 1
	start-stop-daemon --background --start --quiet --make-pidfile --pidfile $PIDFILE \
		--chuid $USER:$GROUP --exec $DAEMON -- \
		$DAEMON_ARGS \
		|| return 2
}

#
# Function that stops the daemon/service
#
do_stop()
{
	# Return
	#   0 if daemon has been stopped
	#   1 if daemon was already stopped
	#   2 if daemon could not be stopped
	#   other if a failure occurred
	start-stop-daemon --stop --quiet --retry=TERM/30/KILL/5 --pidfile $PIDFILE
	RETVAL="$?"
	[ "$RETVAL" = 2 ] && return 2
	# Wait for children to finish too if this is a daemon that forks
	# and if the daemon is only ever run from this initscript.
	start-stop-daemon --stop --quiet --oknodo --retry=0/30/KILL/5 --exec $DAEMON
	[ "$?" = 2 ] && return 2
	# Many daemons don't delete their pidfiles when they exit.
	rm -rf $PIDDIR
	return "$RETVAL"
}

case "$1" in
  start)
    [ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC " "$NAME"
    do_start
    case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
  ;;
  stop)
	[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
	do_stop
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  status)
       status_of_proc -p "$PIDFILE" "$DAEMON" "$NAME" && exit 0 || exit $?
       ;;
  restart|force-reload)
	log_daemon_msg "Restarting $DESC" "$NAME"
	do_stop
	case "$?" in
	  0|1)
		if [ "$1" = "force-reload" ]; then
			# Rescan the collection
			DAEMON_ARGS="$DAEMON_ARGS -R"
		fi
		do_start
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;; # Old process is still running
			*) log_end_msg 1 ;; # Failed to start
		esac
		;;
	  *)
	  	# Failed to stop
		log_end_msg 1
		;;
	esac
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|status|restart|force-reload}" >&2
	exit 3
	;;
esac

:
