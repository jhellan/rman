#!/usr/bin/python
# -*- coding: utf-8 -*-

import errno
import getpass
import os
import shutil
import subprocess
import xml.dom.minidom as minidom


class Serialgarmin(object):
    def __init__(self, model, port):
        self.model = model
        self.port = port

    def upload(self, gpxfile, logger):
        cmd = ["gpsbabel", "-r", "-i", "gpx", "-f", gpxfile,
               "-o", "garmin", "-F", self.port]
        logger.debug('Upload command: "{0}"'.format(str(cmd)))
        try:
            data = subprocess.check_output(cmd)
            if data:
                logger.debug('Upload command output was "{0}"'.format(data))
            os.remove(gpxfile)
        except Exception as exc:
            logger.error('Upload failed - "{0}"'.format(exc))

    @staticmethod
    def probe_port(logger, port="/dev/ttyUSB0"):
        args = ["gpstrans", "-p" + port, "-i"]
        with open("/dev/null") as bitbucket:
            try:
                data = subprocess.check_output(args, stderr=bitbucket)
            except Exception as exc:
                logger.error('{0}'.format(exc))
                return None
        pos = data.find("Connected GPS")
        if pos >= 0:
            data = ' '.join(data[pos:].split()[4:])
            if not data.startswith("Garmin"):
                data = "Garmin " + data
            pos = data.find("Software Version")
            if pos > 0:
                data = data[0:pos]
                return data
        return None

    @staticmethod
    def probe(logger):
        for i in range(0, 9):
            port = "/dev/ttyUSB" + str(i)
            if os.path.exists(port):
                model = Serialgarmin.probe_port(logger, port)
                if model:
                    return model, port
            logger.debug("No device found at " + port)
        return None, None


class Storagegarmin(object):
    def __init__(self, model):
        self.model = model
        self.gpxdir = Storagegarmin.get_garmindir() + "/GPX/"

    def upload(self, gpxfile, logger):
        logger.debug('Upload method: {0}({1},{2})'.format("shutil.move", gpxfile, self.gpxdir))
        try:
            oldfile = os.path.join(self.gpxdir, os.path.basename(gpxfile))
            os.remove(oldfile)
            logger.debug('Route already exists - removed old version ' + oldfile)
        except Exception as exc:
            if isinstance(exc, (IOError, OSError,)) and exc.errno == errno.ENOENT:
                pass  # This is how we tell that the file does not already exist
            else:
                logger.error('Upload failed - couldn\'t remove old version - "{0}"'.format(exc))
                return
        try:
            shutil.move(gpxfile, self.gpxdir)
        except Exception as exc:
            logger.error('Upload failed - "{0}"'.format(exc))
        return

    @staticmethod
    def get_garmindir():
        places = [
            "/var/run/usbmount/Garmin_GARMIN_Flash/Garmin",
            "/media/GARMIN/Garmin",
            "/media/usb/Garmin",
            "/media/{}/GARMIN/Garmin".format(getpass.getuser())
        ]
        for place in places:
            if os.path.isdir(place):
                return place
        return ""

    @staticmethod
    def probe(logger):
        descfile = Storagegarmin.get_garmindir() + "/GarminDevice.xml"
        logger.debug('probing ' + descfile)
        try:
            with open(descfile) as dfh:
                doc = minidom.parse(dfh)
                dref = doc.getElementsByTagName('Device')
                ref = dref[0].getElementsByTagName('Model')
                ref = ref[0].getElementsByTagName('Description')
                model = ref[0].firstChild.nodeValue
                return model
        except Exception as exc:
            if isinstance(exc, (IOError, OSError,)) and exc.errno == errno.ENOENT:
                pass  # This is how we tell that nothing is connected
            else:
                logger.error('{0}'.format(exc))
            return None
