#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Loops, looking for navigators at the USB ports, and gpx files in
# /var/run/rman/<model>.gps. The gpx files are copied to the navigators

import argparse
import errno
import logging
import os
import shutil
import string
import time
from rmangarmin import Serialgarmin, Storagegarmin

RSUFF = "gps"
OUTGOING = "outgoing"


class Rmanuploader(object):

    def __init__(self, rdir, pollint, logger):
        self.rdir = rdir
        self.pollint = pollint
        self.logger = logger
        self.navigators = {}
        self.last_navigators = {}

    def get_navigator(self):
        (model, port) = Serialgarmin.probe(self.logger)
        if model:
            navigator = Serialgarmin(model, port)
        else:
            model = Storagegarmin.probe(self.logger)
            if model:
                navigator = Storagegarmin(model)
            else:
                navigator = None
        if model:
            self.logger.debug('{} detected'.format(model))
        else:
            self.logger.debug('No navigator detected')
        return navigator

    def ensure_navigator_dir(self, mynavigator):
        basename = make_basename(mynavigator.model)
        if basename not in self.last_navigators:
            self.logger.info('Unit {0} detected'.format(mynavigator.model))
        self.navigators[basename] = mynavigator
        dirname = self.rdir + "/" + basename + "." + RSUFF
        try:
            os.mkdir(dirname)
        except Exception as exc:
            if (isinstance(exc, OSError) and
                    exc.errno == errno.EEXIST and os.path.isdir(dirname)):
                pass  # Existed already
            else:
                self.logger.error('mkdir failed - "{0}"'.format(exc))
                raise
        with open(dirname + "/" + "model.txt", "w") as mfh:
            mfh.write(mynavigator.model + "\n")  # So that webapp can show it

    def handle_removed_navigator(self, navigator_name, absdir):
        try:
            model = self.last_navigators[navigator_name].model
        except:
            model = navigator_name
        self.logger.info('Unit {0} disconnected'.format(model))
        try:
            shutil.rmtree(absdir)
            self.logger.debug("Removed directory " + absdir)
        except Exception as exc:
            self.logger.error(
                "Unable to remove {0} - {1}".format(absdir, exc))

    def get_navigator_dirs(self):
        return [dir_ for dir_ in os.listdir(self.rdir)
                if dir_.endswith(('.' + RSUFF))]

    def get_files_to_upload(self, absdir):
        upload_files = [file_ for file_ in os.listdir(absdir)
                        if file_.endswith('.gpx') or file_.endswith('.GPX')]
        if upload_files:
            self.logger.debug('Maildrop contains "{0}"'.format(str(upload_files)))
        return upload_files

    def move_file(self, file_, srcdir, dstdir):
        dstdir = srcdir + "/" + OUTGOING
        try:
            os.mkdir(dstdir)
        except Exception as exc:
            if (isinstance(exc, OSError) and
                    exc.errno == errno.EEXIST and os.path.isdir(dstdir)):
                pass  # Existed already
            else:
                self.logger.error(
                    'mkdir failed - "{0}"'.format(exc))
                raise
        try:
            shutil.move(srcdir + "/" + file_, dstdir)
        except Exception as exc:
            self.logger.error(
                'shutil.move failed - "{0}"'.format(exc))

    def upload_file(self, absfile, navigator_name):
        self.navigators[navigator_name].upload(absfile, self.logger)
        self.logger.info('{0} uploaded'.format(absfile))

    def run_once(self):
        mygarmin = self.get_navigator()
        if mygarmin:
            self.ensure_navigator_dir(mygarmin)

        for dir_ in self.get_navigator_dirs():
            absdir = self.rdir + "/" + dir_
            navigator_name, _, _ = dir_.partition(".")
            if navigator_name in self.navigators:
                upload_files = self.get_files_to_upload(absdir)
                if upload_files:
                    outdir = absdir + "/" + OUTGOING
                    for file_ in upload_files:
                        self.move_file(file_, absdir, outdir)
                    for outfile in os.listdir(outdir):
                        self.upload_file(outdir + '/' + outfile, navigator_name)
            else:
                self.handle_removed_navigator(navigator_name, absdir)

    def run(self):
        self.logger.info("Starting")
        self.last_navigators = {}
        while True:
            self.navigators = {}
            self.run_once()
            self.last_navigators = self.navigators
            time.sleep(self.pollint)


def make_basename(model):
    # Filter downcases, then removes everything outside "a-z0-9"
    return "".join(s for s in model if s in string.digits + string.letters).lower()


DESCRIPTION = "GPS route uploader"


def parse_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--rundir', default='/var/run/rman',
                        help='Runfile directory')
    parser.add_argument('--logfile', help='Log file')
    parser.add_argument('--pollint', default=0.5, type=float,
                        help='Polling interval')
    return parser.parse_args()


def main():
    args = parse_args()
    logfmt = '%(asctime)s %(name)s %(message)s'
    datefmt = '[%d/%b/%Y:%H:%M:%S]'
    if args.logfile:
        logging.basicConfig(format=logfmt, datefmt=datefmt,
                            level=logging.INFO, filename=args.logfile)
    else:
        logging.basicConfig(format=logfmt, datefmt=datefmt,
                            level=logging.DEBUG)  # Log to stdout
    logger = logging.getLogger('UPLOADER')
    Rmanuploader(args.rundir, args.pollint, logger=logger).run()


if __name__ == '__main__':
    main()
