# Rman - A GPS Route Manager

- Store routes for a chart plotter or GPS device as GPX files in a
  database.
- Web application to choose routes to upload to the chart plotter.
- Access web application from a computer, tablet or smartphone.
- Enter routes by uploading to the web application from a URL or a file.
- In particular, upload routes shared by Navionics on tablets/phones,
  by using a link in the email Navionics sends.

We run the software on a Raspberry Pi with a wireless adapter, using
Raspbian. Other Linuxes should be easy. OS/X would require moderate
work - paths and device names differ from Linux.


## GPS devices

The system has been tested with the following GPS devices:
- Garmin Oregon: Mounts as USB storage. Write gpx files.
- Garmin GPSmap 182c: Garmin protocol over RS-232 adapter and /dev/ttyUSB0.
- Garmin  eTrex Vista HCx: Garmin protocol over USB cable and /dev/ttyUSB0.

## Web application

- Python 2.x
- cherrypy - pip install
- Sqlalchemy
- sqlite database
- Mako templates
- Bootstrap 3
- jquery
- nginx reverse proxy

## Talking to the GPS device

The web application writes a gpx file to a well known place. A
script runs in an endless loop, checking for GPS devices and gpx
files, and uploads when possible.

## Required Raspbian packages

For the web application

- python-pip
- nginx-light
- build-essential (comes with Raspbian)
- gpsbabel

For talking to the GPS device

- gpstrans

For debugging

- sqlite3
- openssh-server (comes with Raspbian)

For maintenance

- git (comes with Raspbian)

## Issues

Offline, the Pi will not know time. Possible time sources:

- Timestamp on incoming routes
- Connected GPS

## Installation

Here is how to install on a Raspberry Pi running Raspbian. The Pi
needs an internet connection during installation. The system is
able to work offline once installation is complete.

You need a user `rman`.

Web application

- `webapp/rman.py`, `webapp/data/` and `webapp/views/` go in
  `/usr/local/lib/rman/`. `rman.py` needs `+x` access.
- `webapp/init.d` becomes `/etc/init.d/rman`. Needs `+x` access.
  Needs `update-rc.d rman defaults`.
- Necessary Python modules are installed using `pip`-
      apt-get install python-pip
      pip install -r webapp/requirements.txt
  
Uploader

- `uploader/rmanuploader.py` and `uploader/rmangarmin.py` go in
  `/usr/local/lib/rman/uploader`. `rmanuploader.py` needs `+x` access.
- `uploader/init.d` becomes `/etc/init.d/rman`. Needs `+x` access.
  Needs `update-rc.d rmanuploader defaults`.

Config files for Raspbian packages.

These instructions are for a box that will only run rman.

- Install the packages listed in 'Required Raspbian packages'.
- `nginx/sites-available/default` goes in
  `/etc/nginx/sites-available`. There should be a symlink
  `/etc/nginx/sites-enabled/default` pointing at it.
- `wifi/network/interfaces` goes in `/etc/network`.

No longer in use:

- `postfix/aliases`
- `wifi/hostapd/hostapd.conf`
- `wifi/dnsmasq/dnsmasq.conf`
