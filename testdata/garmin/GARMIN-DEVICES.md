# Garmin devices
 
- Garmin Oregon: gpx files. Mounts as USB storage
- Garmin GPSmap 182c: Garmin protocol. Serial comms over /dev/ttyUSB0.
- Garmin  eTrex Vista HCx: Garmin protocol. Serial comms over /dev/ttyUSB0.

How to detect old style Garmin: 

    gpstrans -p/dev/ttyUSB0  -i
e.g.

    $ gpstrans -p/dev/ttyUSB0  -i  2>/dev/null | grep "Connected GPS"
    Connected GPS [/dev/ttyUSB0] is: Garmin GPSMAP 182C Software Version - V6.2
or

    Connected GPS [/dev/ttyUSB1] is: Garmin eTrex Vista HCx Software Version - V3.2

Blocks 10 seconds if /dev/ttyUSB0 exists but there is no GPS:

    Connected GPS [/dev/ttyUSB0] is: <GPS not responding>
If we try this too soon after connecting:

    ERROR:  The initialization of port /dev/ttyUSB1 has failed.

Routes on the Oregon are at `<mount point>/Garmin/GPX`. On Raspbian, the
symlink `/var/run/usbmount/Garmin_GARMIN_Flash` points to the mount
point, so we can use `/var/run/usbmount/Garmin_GARMIN_Flash/Garmin/GPX/`. 

Model info is in `GarminDevice.xml` in the root directory of the
Oregon. The included `GarminDevice.xml` is an example, and
`GarminDevice_pretty.xml` is the same data pretty-printed for
legibility.

This file can also be used to find the directory for GPX files. Look
for the `Path` tag. For now, we just use a hardcoded value.
